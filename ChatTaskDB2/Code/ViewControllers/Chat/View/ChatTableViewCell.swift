//
//  ChatTableViewCell.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Alamofire
import SwipeCellKit

class ChatTableViewCell: SwipeTableViewCell, TableViewCellConfigurable {
    
    var model: Channel?
    fileprivate let dateFormatter = DateFormatter()
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unreadCountLabel: UnreadLabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userThumbImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userThumbImage?.makeRounded()
    }
    
    func configure(with model: Channel) {
        self.timeLabel.text = model.lastMessage.messageDate()
        self.lastMessageLabel.text = model.lastMessage.text()
        
        guard let companion = model.getCompanion() else {return }
        self.userThumbImage.imageFrom(url: companion.imageUrl)
        self.userNameLabel.text = companion.fullName

        self.unreadCountLabel.isHidden = !(model.unreadCount > 0)
        self.unreadCountLabel.text = String(model.unreadCount)
    }
    
    override func prepareForReuse() {
        userThumbImage.image = nil
        lastMessageLabel.text = nil
        userNameLabel.text = nil
        unreadCountLabel.text = nil
        timeLabel.text = nil
    }
}
