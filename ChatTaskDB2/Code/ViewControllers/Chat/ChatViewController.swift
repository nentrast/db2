//
//  ChatViewController.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatType: DBSegmentControll!
    fileprivate var datasource: TableViewDataSource?
    var chatSevice: ChatService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setupDatasource()
        setupSegmentConrtoll()
    }
    
    func showMessagesViewControler(for channel: Channel) {
        let destination = MessagesViewController()
        destination.chatService = chatSevice
        destination.channel = channel
        destination.senderId = "1947"
        destination.senderDisplayName = "name"
        navigationController?.pushViewController(destination, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
        getChannels()
    }
}

private extension ChatViewController {
    func getChannels() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        chatSevice?.channels(resultHandler: { (chanels) in
            var unreadChannels = [Channel?]()
            var readChanels = [Channel?]()
            for channel in chanels {
                if (channel?.unreadCount)! > 0 {
                    unreadChannels.append(channel)
                } else {
                    readChanels.append(channel)
                }
            }
            let unreadCount = unreadChannels.reduce(0, { (unreadMessages, channelMsg) -> Int in
                unreadMessages + channelMsg!.unreadCount
            })
            self.chatType.setUnraedCount(messages: unreadCount) 
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.datasource?.updateWith(objects: [ChannelSection(items: unreadChannels as! [TableViewCompatible]), ChannelSection(items: readChanels as! [TableViewCompatible])])
        }) { (error) in
            print(error)
        }
        
    }
    
    func setupDatasource() {
        datasource = TableViewDataSource(tableView: tableView, items: [])
        datasource?.delegate.channelClossure = showMessagesViewControler
        chatSevice = ChatService(login: "iostest", password: "iostest2k17!")
    }
    
    func setupNavigationController() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = #imageLiteral(resourceName: "TransparentPixel")
        navigationController?.navigationBar.setBackgroundImage(UIImage(color: UIColor.dbdarkSkyBlue), for: .default)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationItem.titleView?.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
    }
    
    func setupSegmentConrtoll() {
        chatType.items = ["Chat","Live chat"]
        chatType.backgroundColor = UIColor.dbDeepBlue
        chatType.borderColor = .clear
        chatType.unselectedLabelColor = UIColor.lightGray
        chatType.selectedLabelColor = UIColor.darkGray
    }
}


extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
