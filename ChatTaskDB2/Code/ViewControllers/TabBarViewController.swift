//
//  TabBarViewController.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/29/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.barTintColor = UIColor.dbDeepBlue
    }
}
