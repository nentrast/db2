//
//  MessagesViewController.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/30/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class MessagesViewController: JSQMessagesViewController {
    
    var chatService: ChatService?
    var messages = [Message]()
    var channel: Channel?

    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyScrollsToMostRecentMessage = true
        collectionView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        title = channel?.getCompanion()?.fullName
        let blockItem =  UIBarButtonItem(title: "Block", style: .done, target: self, action: #selector(blockUser))
        navigationItem.rightBarButtonItem = blockItem
    }
    
    func blockUser() {
        print(" you try to block \(channel?.getCompanion()?.fullName)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        chatService?.messages(for: nil, resultHandler: { (messages) in
            self.messages = messages as! [Message]
            self.channel?.unreadCount = 0
            self.collectionView.reloadData()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }, errorHandler: { (error) in
            print(error)
        })
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        messages.append(Message(text: text, user: User(id: Int(senderId)!, imageUrl: "", firstName: senderDisplayName, lastName: "", nickName: ""), create: date, isRead: false))
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: messages.count - 1, section: 0), at: UICollectionViewScrollPosition.bottom, animated: true)
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        print("accessory presed")
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if String(message.sender.userID) == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        cell.cellBottomLabel.text = message.messageTime()
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        guard String(message.sender.userID) == senderId else {
            return JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.white)
        }
        return JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.dbdarkSkyBlue)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.item]
        guard String(message.sender.userID) == senderId else {
            return JSQMessagesAvatarImageFactory.avatarImage(with: message.sender.image, diameter: 44)
        }
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        guard messages[indexPath.item].create.isToday() else {
            return NSAttributedString(string: messages[indexPath.item].timeStamp())
        }
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        guard messages[indexPath.item].create.isToday() else {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        return 0.0
    }

}
