//
//  Channel.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Argo
import Curry
import Runes
import SwipeCellKit

//MARK: - user own id
let ownId: Int = 1947

class Channel: NSObject, TableViewCompatible {
    var reuseIdentifier: String = "ChatUserCell"
    
    let id: Int
    let users: [User]
    let lastMessage: Message
    var unreadCount: Int

    init(id: Int, users: [User], lastMassege: Message, unreadCount: Int) {
        self.id = id
        self.users = users
        self.lastMessage = lastMassege
        self.unreadCount = unreadCount
    }
    
    func getCompanion() -> User? {
        return users.filter({ $0.userID != ownId }).first
    }
 
    func cellForTableView(tableView: UITableView, atIndexPath indexPath: IndexPath) -> SwipeTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? ChatTableViewCell else { return SwipeTableViewCell() }
        cell.configure(with: self)
        return cell
    }
}

extension Channel: Decodable {
    static func decode(_ json: JSON) -> Decoded<Channel> {
        return curry(Channel.init)
            <^> json <|  "id"
            <*> json <|| "users"
            <*> json <|  "last_message"
            <*> json <|  "unread_messages_count"
    }
}

class ChannelSection: TableViewSectionCompatible {
    var items: [TableViewCompatible]
    init(items: [TableViewCompatible]) {
        self.items = items
    }
}
