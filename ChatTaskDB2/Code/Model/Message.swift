//
//  Message.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Argo
import Curry
import Runes
import Timepiece
import JSQMessagesViewController

class Message: NSObject {
    
    let textMessage: String
    let sender: User
    let create: Date
    let isRead: Bool
    var reuseIdentifier: String = "messageCell"
    
    fileprivate let dateFormatter = DateFormatter()
    
    init(text: String, user: User, create: String, isRead: Bool) {
        dateFormatter.locale = Locale.current
        self.textMessage = text
        self.sender = user
        self.create = dateFormatter.toDate(string: create) ?? Date()
        self.isRead = isRead
    }
    
    init(text: String, user: User, create: Date, isRead: Bool) {
        dateFormatter.locale = Locale.current
        self.textMessage = text
        self.sender = user
        self.create = create
        self.isRead = isRead
    }
    
    func messageTime() -> String {
        return dateFormatter.toString(date: create, format: "hh:mm")
    }
    
    func timeStamp() -> String {
        return dateFormatter.toString(date: create, format: "dd MMM", relative: true)
    }

    func messageDate() -> String {
        switch create {
        case let date where date.isToday():
            return dateFormatter.toString(date: create, format: "hh:mm")
        case let date where date.isYesterday():
            return dateFormatter.toString(date: create, format: "dd MMM, HH:mm", relative: true)
        default:
            return dateFormatter.toString(date: create, format: "dd MMM, HH:mm")
        }
    }
}

extension Message: JSQMessageData {
    func senderId() -> String! {
        return "\(self.sender.userID)"
    }
    
    func senderDisplayName() -> String! {
        return self.sender.fullName
    }
    
    func date() -> Date! {
        return create
    }
    
    func isMediaMessage() -> Bool {
        return false
    }
    
    func text() -> String! {
        return textMessage
    }
    
    func messageHash() -> UInt {
//        let contentHash = self.isMediaMessage() ? self.sender.imageUrl.hash : self.textMessage.hash
        return UInt(9283509837459)
    }

}

extension Message: Decodable {
    static func decode(_ json: JSON) -> Decoded<Message> {
        return curry(Message.init)
            <^> json <| "text"
            <*> json <| "sender"
            <*> json <| "create_date"
            <*> json <| "is_read"
    }
}
