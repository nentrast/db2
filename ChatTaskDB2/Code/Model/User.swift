//
//  User.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Argo
import Curry
import Runes
import Alamofire

class User {
    let userID: Int
    let firstName: String
    let lastName: String
    let nickName: String
    let imageUrl: String
    var image: UIImage?
    
    lazy var fullName: String = { [unowned self] in
        return "\(self.firstName) \(self.lastName)"
    }()

    init(id: Int, imageUrl: String, firstName: String, lastName: String, nickName: String) {
        self.userID = id
        self.imageUrl = imageUrl
        self.firstName = firstName
        self.lastName = lastName
        self.nickName = nickName
        self.image = UIImage(color: UIColor.gray)
    }
}

extension User: Decodable {
    static func decode(_ json: JSON) -> Decoded<User> {
        return curry(User.init)
            <^> json <| "id"
            <*> json <| "photo"
            <*> json <| "first_name"
            <*> json <| "last_name"
            <*> json <| "username"
    }
}
