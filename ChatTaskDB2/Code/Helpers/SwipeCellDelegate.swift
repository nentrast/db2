//
//  SwipeCellDelegate.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/29/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import SwipeCellKit

class SwipeCellDelegate: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .left else { return nil }
        
        let deleteAction = SwipeAction(style: .default, title: "Remove") { action, indexPath in
            print("Remove")
        }
        deleteAction.backgroundColor = UIColor.dbdarkSkyBlue
        return [deleteAction]
    }
    
}
