//
//  NetworkService.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/28/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Alamofire

class Request {
    let path: String
    let params: [String : String]?
    var headers: [String : String]?
    
    init(path: String, params: [String : String]? = nil) {
        self.path = path
        self.params = params
    }
}

class NetworkService: NSObject {
    func performRequest(request: Request, method: Alamofire.HTTPMethod, responseHandler: @escaping ((NSDictionary) -> Void), errorHandler: @escaping ((NSError) -> Void)) {
        Alamofire.request(request.path , method: .get, parameters: request.params, encoding: JSONEncoding.default, headers: request.headers).validate().responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                errorHandler(NSError.dataError(description: error.localizedDescription))
            case .success(let responseObject):
                guard let dictionary = responseObject as? NSDictionary else {
                    errorHandler(NSError.jsonSerializationFailed)
                    return
                }
                responseHandler(dictionary)
            }
        }
    }
    
    func loadImage(url: String, image: @escaping (UIImage?) -> Void) {
        Alamofire.request(url).responseData { (responseData) in
            guard let data = responseData.data else { image(UIImage(color: .gray)); return }
            image(UIImage(data: data))
        }
    }
}
