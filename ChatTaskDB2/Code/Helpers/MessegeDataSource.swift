//
//  MessegeDataSource.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 8/1/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class MessegeDataSource: NSObject, JSQMessagesCollectionViewDataSource {
    
    fileprivate let collectionView: JSQMessagesCollectionView
    fileprivate var messages = [Message]()
    
    func senderId() -> String! {
        return "1974"
    }
    
    func senderDisplayName() -> String! {
        return "sender Name"
    }
    
    init(collectionView: JSQMessagesCollectionView, messages: [Message]) {
        self.collectionView = collectionView
        super.init()
        self.collectionView.dataSource = self
    }

    func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if String(message.sender.userID) == senderId() {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        cell.cellBottomLabel.text = message.messageTime()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    func collectionView(_ collectionView: UICollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        guard String(message.sender.userID) == senderId() else {
            return JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.white)
        }
        return JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.dbdarkSkyBlue)
    }
    
    func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.item]
        guard String(message.sender.userID) == senderId() else {
            return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(color: UIColor.dbDeepBlue), diameter: 44)
        }
        return nil
    }
    
    func update(messages: [Message]) {
        self.messages = messages
        self.collectionView.reloadData()
    }

}
