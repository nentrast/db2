//
//  Date+Extension.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 8/1/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import Foundation

extension Date {
    func isToday() -> Bool {
        return isSameDay(Date.today())
    }
    
    func isYesterday() -> Bool {
        return isSameDay(Date.yesterday())
    }
    
    func isSameDay(_ date: Date) -> Bool {
        return
            date.day == self.day &&
                date.month == self.month &&
                date.year == self.year
    }
}
