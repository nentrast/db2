//
//  UIImageView + Extension.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 8/1/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Alamofire
import Async

extension UIImageView {
    func imageFrom(url: String) {
        
        Async.userInitiated ({
            Alamofire.request(url).responseData(completionHandler: { response  in
                guard let data = response.data else {
                    self.image = UIImage(color: .gray)
                    return
                }
                Async.main({
                    self.image = UIImage(data: data)
                })
            })
        })
    }
    
    func makeRounded() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}
