//
//  DateFormatter+Extension.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 8/1/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import Foundation

extension DateFormatter {
    func toDate(string: String) ->  Date? {
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        return date(from: string)
    }
    func toString(date: Date, format: String? = nil, relative: Bool = false) -> String {
        dateStyle = .medium
        locale = Locale.current
        doesRelativeDateFormatting = relative
        dateFormat = format
        return string(from: date)
    }
}
