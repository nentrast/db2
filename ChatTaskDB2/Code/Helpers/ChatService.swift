//
//  ChatService.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/28/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import Argo

class ChatService: NSObject {
    fileprivate let login: String
    fileprivate let password: String
    fileprivate let networkService = NetworkService()
    
    init(login: String, password: String) {
        self.login = login
        self.password = password
    }
    
    func channels(resultHandler:@escaping ([Channel?]) -> Void, errorHandler:@escaping ((Error) -> Void)) {
        let req = request(path: "https://iostest.db2dev.com/api/chat/channels/")
        networkService.performRequest(request: req, method: .get, responseHandler: { dictionary in
            guard let rawChannels = dictionary["channels"] as? NSArray else {
                errorHandler(NSError.jsonSerializationFailed)
                return
            }
            resultHandler(rawChannels.map({ (object) -> Channel? in
                let dictionary = JSON(object)
                return Channel.decode(dictionary).value
            }))
            
        }) { error in
            errorHandler(error)
        }
    }
    
    func messages(for channel: Channel?, resultHandler: @escaping ([Message?])-> Void, errorHandler: @escaping ((Error) -> Void)) {
        let req = request(path: "https://iostest.db2dev.com/api/chat/channels/1/messages/")
        networkService.performRequest(request: req, method: .get, responseHandler: { dictionary in
            guard let rawMessages = dictionary["messages"] as? NSArray else {
                errorHandler(NSError.jsonSerializationFailed)
                return
            }
            resultHandler( rawMessages.map({ object -> Message? in
                let dictionary = JSON(object)
                return Message.decode(dictionary).value

            }))
        }) { (error) in
            errorHandler(error)
        }
    }
}

fileprivate extension ChatService {
    func request(path: String) -> Request {
        let request = Request(path: path)
        let credentialData = "\(login):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString()
        request.headers = ["Authorization": "Basic \(base64Credentials)"]
        return request
    }
}

