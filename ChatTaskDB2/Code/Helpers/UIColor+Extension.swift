//
//  UIColor.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/29/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

extension UIColor {

    class var dbDeepBlue: UIColor {
        return getColor(red: 58, green: 79, blue: 152)
    }
    
    class var dbgreyishBrown: UIColor {
        return getColor(red: 74, green: 74, blue: 74)
    }
    
    class var dbdarkSkyBlueTwo: UIColor {
        return getColor(red: 80, green: 195, blue: 227)
    }
    
    class var dbdarkSkyBlue: UIColor {
        return getColor(red: 74, green: 144, blue: 226)
    }
    
    class fileprivate func getColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1)
    }
}
