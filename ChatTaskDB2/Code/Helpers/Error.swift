//
//  Error.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/28/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import Foundation

enum ErrorCode: Int {
    case unknown = 0
    case statusCodeValidationFailed = 1
}

enum NSError: Error {
    case failedJson
    case networkError(ErrorCode)
    case jsonSerializationFailed
    case dataError(description: String)
}

extension NSError {
    var localizedDescription: String {
        switch self {
        case .dataError(let desctiprion):
            return desctiprion
        default:
            return "Unknown error"
        }
    }
}
