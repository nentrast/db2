//
//  TableViewDelegate.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 8/2/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

class TableViewDelegate: NSObject, UITableViewDelegate {
    fileprivate let tableView: UITableView
    var items: [TableViewSectionCompatible]
    var channelClossure: ((Channel) -> Void)?
    
    init(tableView: UITableView, items: [TableViewSectionCompatible]) {
        self.tableView = tableView
        self.items = items
        super.init()
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return tittleFor(section: section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tittleFor(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return heightFor(section: section)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightFor(section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        channelClossure?(items[indexPath.section].items[indexPath.row] as! Channel)
    }
}

extension TableViewDelegate {
    func heightFor(section: Int) -> CGFloat{
        guard section == 0 else {
            return 0.0
        }
        return 5.0
    }
    
    func tittleFor(section: Int) -> String? {
        guard section == 0 else {
            return nil
        }
        return " "
    }
}
