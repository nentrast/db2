//
//  ExtendedNavBar.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/29/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

class ExtendedNavBar: UIView {
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        layer.shadowOffset = CGSize(width: 0, height: CGFloat(1) / UIScreen.main.scale)
        layer.shadowRadius = 0

        layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        layer.shadowOpacity = 0.25
        backgroundColor = UIColor.dbdarkSkyBlue
    }

}
