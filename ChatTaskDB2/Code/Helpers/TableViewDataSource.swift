//
//  TableViewDataSource.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/27/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol TableViewCellConfigurable {
    associatedtype T
    var model: T? { get set }
    func configure(with model: T)
}

protocol TableViewSectionCompatible {
    var items: [TableViewCompatible] { get set }
}

protocol TableViewCompatible {
    var reuseIdentifier: String { get }
    func cellForTableView(tableView: UITableView, atIndexPath indexPath: IndexPath) -> SwipeTableViewCell
}

class TableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    enum SectionType : Int {
        case unread = 0
        case read
    }
    
    fileprivate let tableView: UITableView
    let delegate: TableViewDelegate
    fileprivate var items: [TableViewSectionCompatible]
    fileprivate var celldelegate = SwipeCellDelegate()
    
    init(tableView: UITableView, items: [TableViewSectionCompatible]) {
        self.delegate = TableViewDelegate(tableView: tableView, items: items)
        self.tableView = tableView
        self.items = items
        super.init()
        self.tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = items[indexPath.section]
        let cell = model.items[indexPath.row].cellForTableView(tableView: tableView, atIndexPath: indexPath)
        cell.delegate = celldelegate
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int  {
        return items.count
    }
    
    func updateWith(objects: [TableViewSectionCompatible]? = nil ) {
        guard let items = objects else {
            tableView.reloadData()
            return
        }
        self.items = items
        delegate.items = items
        tableView.reloadData()
    }
}


