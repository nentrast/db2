//
//  UnreadLabel.swift
//  ChatTaskDB2
//
//  Created by Alexandr Lobanov on 7/29/17.
//  Copyright © 2017 Alexandr Lobanov. All rights reserved.
//

import UIKit

class UnreadLabel: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit() {
        layer.cornerRadius = self.bounds.width / 2
        clipsToBounds = true
        textColor = UIColor.white
        backgroundColor = UIColor.dbdarkSkyBlueTwo
        textAlignment = .center
    }
}
